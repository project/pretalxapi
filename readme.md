# Pretalx API !

## INTRODUCTION

"Pretalx API!" example in Drupal.
Drupal 8 module with a controller.

## REQUIREMENTS

The module does not have any specific requirements.

## INSTALLATION

Install as you would normally install a Drupal module.

Please download and install through Drupal admin area
or use `composer require drupal/pretalxapi,
then enable the "PretalxApi" D9 module.

## CONFIGURATION

 * Configure the PretalxApi module at (/admin/config/pretalxapi/configure).
 * After configure ,publish/enable the new created field(`speaker`) at (/admin/config/people/accounts/form-display)

MAINTAINERS
-----------

Current maintainers:

This project has been sponsored by: