<?php 
namespace Drupal\pretalxapi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class PretalxApiConfigForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'pretalxapi.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pretalxapi_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['pretalxapi_endpoint'] = [
      '#type' => 'url',
      '#title' => $this->t('Endpoint Domain'),
		'#required' => TRUE,
      '#default_value' => $config->get('pretalxapi_endpoint'),
    ];  

    $form['pretalxapi_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pretalx Api Token'),
		'#required' => TRUE,
      '#default_value' => $config->get('pretalxapi_token'),
    ];  

    return parent::buildForm($form, $form_state);
  }

/**
 * {@inheritdoc}
 */
public function validateForm(array &$form, FormStateInterface $form_state) {
  if (strlen($form_state->getValue('pretalxapi_endpoint')) < 3) {
    $form_state->setErrorByName('pretalxapi_endpoint', $this->t('Endpoint domain is required!'));
  }
  if (strlen($form_state->getValue('pretalxapi_token')) < 3) {
    $form_state->setErrorByName('pretalxapi_token', $this->t('API token is required'));
  }

}
  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('pretalxapi_endpoint', $form_state->getValue('pretalxapi_endpoint'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      ->set('pretalxapi_token', $form_state->getValue('pretalxapi_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}